#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created 2023

@author: S.N.M.
"""



import argparse, pathlib, os, re, dendropy, subprocess
import pandas as pd
import numpy as np
import Bio
from Bio import SeqIO
from collections import defaultdict
from argparse import RawTextHelpFormatter
import requests
from Bio import GenBank
import pickle 
import urllib.request
import gzip
import shutil
import configparser




parser = argparse.ArgumentParser(description='\tDescription:\n\tPAPAplastome v.01 - This program incorporates your newly sequenced and annotated plastome data into a phylogenetic tree with reference data from the NCBI RefSeq database which is automatically downloaded.\n\tcontact: s.meckoni@tu-bs.de',formatter_class=RawTextHelpFormatter)
parser.add_argument('work_dir', type=pathlib.Path, metavar='INPUT', help='Path to working directory folder')
parser.add_argument('config_file', type=pathlib.Path, metavar='CONFIG', help='Path to config file')

parser.add_argument('-n','--no_tree', action='store_false', help='Do not calculate a phylogenetic tree.')
parser.add_argument('-d','--re_download', action='store_true', help='Downloads the RefSeq files even if they are present. This can be helpful if the integrity of the RefSeq files might be corrupted.')
parser.add_argument('-l','--re_load', action='store_true', help='Re-Loads the data from the RefSeq files. This should be conducted if the config file was modified. This will be automatically enabled if -d is used.')


args = parser.parse_args()



def argument_using_and_function_caller( args ):
    print()



    # arguments into variables:
    
    no_tree = args.no_tree

    re_load = args.re_load

    re_download = args.re_download

    work_dir = args.work_dir



    # load data from config file into variables:

    db_dir, orthofinder_Path, iqtree_path, iqtree_params, input_files_path, ncbi_refseq_link, reference_sp_lst, outgroup_sp_lst, crs_lists_lst = read_config(args.config_file)


    # Download the RefSeq Databse

    get_refseq_gbff(db_dir, re_download, ncbi_refseq_link)


    # load the GenBank from the RefSeq database into a python dict

    genbank_to_dict(db_dir,work_dir,re_load,reference_sp_lst)


    # harmonizing = "translate" NCBI ids into species names

    harmonized_crs_lists_lst, harmonized_outgroup_sp_lst = load_pep_from_dict(work_dir,crs_lists_lst,outgroup_sp_lst)

    copy_to_pep_folder(work_dir,input_files_path)
    
    new_crs_lst = check_crs(work_dir, harmonized_crs_lists_lst)


    # the enumeration of the peptide IDs is important. Otherwise Dendropy would not work.

    numerate_ids_2(work_dir)

    call_orthofinder2(work_dir, orthofinder_Path)



    # The trimming of the Orthofinder results is an important step of this program

    trimm_of2_results(work_dir, new_crs_lst, harmonized_outgroup_sp_lst)


    
    mafft_loop(work_dir)
    
    concatenation( work_dir, iqtree_path, iqtree_params, no_tree )
    


    pass



#pep_prep_and_OF_run


def read_config(config_path):
    config = configparser.ConfigParser()
    config.read(config_path)
    config.sections()


    db_dir = pathlib.Path(config['Paths']['DataBank_folder'].strip())

    orthofinder_Path=pathlib.Path(config['Paths']['OrthoFinder'].strip())

    iqtree_path = pathlib.Path(config['Paths']['IQ-Tree'].strip())
    
    iqtree_params = config['Paths']['IQ-Tree_params'].strip()

    input_files_path = pathlib.Path(config['Paths']['Input_files'].strip())

    ncbi_refseq_link = config['Paths']['NCBI_RefSeq_link'].strip()


    reference_sp_lst=config['Define_species']['Reference_species'].strip().replace(' ','').split(',')
    
    outgroup_sp_lst=config['Define_species']['Outgroup_species'].strip().replace(' ','').split(',')

    reference_sp_lst=list(set(reference_sp_lst+outgroup_sp_lst)) # the final species list are the unique entries of the Reference and the 
    

    #close-related-species lists containing list:
    crs_lists_lst = []
    for element in config['CRS']:
        sub_lst = config['CRS'][element].strip().replace(' ','').split(',')
        if len(sub_lst) >= 2:
            crs_lists_lst.append(sub_lst)
        else:
            print('Alert! Found only one entry in config file under "',element,'" in the "[CRS]" section. It will not be used for further analysis.','\n')

    return db_dir, orthofinder_Path, iqtree_path, iqtree_params, input_files_path, ncbi_refseq_link, reference_sp_lst, outgroup_sp_lst, crs_lists_lst


def get_refseq_gbff(db_dir, re_download, ncbi_refseq_link):
    #make dirs
    refseq_release_no = requests.get('https://ftp.ncbi.nlm.nih.gov/refseq/release/RELEASE_NUMBER').text.strip()
    print('The newest RefSeq release is no.: ', refseq_release_no, '\n')

    refseq_raw_data_folder = db_dir / str('Release_no_' + refseq_release_no) / 'gzipped'
    if refseq_raw_data_folder.parent.is_dir() == False:
        os.mkdir(refseq_raw_data_folder.parent)
        os.mkdir(refseq_raw_data_folder)
    elif refseq_raw_data_folder.is_dir() == False:
        os.mkdir(refseq_raw_data_folder)

    refseq_raw_data_folder_gunzipped = db_dir / str('Release_no_' + refseq_release_no) / 'gunzipped'
    if refseq_raw_data_folder_gunzipped.is_dir() == False:
        os.mkdir(refseq_raw_data_folder_gunzipped)
    
    ftp_file_list = requests.get(ncbi_refseq_link).text.split('</a>')
    for i in ftp_file_list:
        if 'gz' in i:
            ftp_file_name = i.split('>')[-1]
            if 'genomic.gbff' in ftp_file_name:
                if (refseq_raw_data_folder / ftp_file_name).is_file() == False or re_download == True:
                    print('File with the name ', ftp_file_name, ' is now being downloaded...\n')
                    try:
                        with urllib.request.urlopen(ncbi_refseq_link+ftp_file_name) as response, open(refseq_raw_data_folder / ftp_file_name, 'wb') as out_file:
                            shutil.copyfileobj(response, out_file)
                    except:
                        print('Error downloading file, trying again...')
                        if (refseq_raw_data_folder / ftp_file_name).is_file():
                            os.remove(refseq_raw_data_folder / ftp_file_name)
                        with urllib.request.urlopen(ncbi_refseq_link+ftp_file_name) as response, open(refseq_raw_data_folder / ftp_file_name, 'wb') as out_file:
                            shutil.copyfileobj(response, out_file)
                    #urllib.request.urlretrieve(str(ncbi_refseq_link+ftp_file_name), (refseq_raw_data_folder / ftp_file_name))
                    print('Download finished.','\n')
                    print('Now un-compressing file...')
                    print('.'.join(str(refseq_raw_data_folder / ftp_file_name).split('.')[0:-1]))
                    with gzip.open(refseq_raw_data_folder / ftp_file_name, 'rb') as f_in:
                        with open('.'.join(str(refseq_raw_data_folder_gunzipped / ftp_file_name).split('.')[0:-1]), 'wb') as f_out:
                            shutil.copyfileobj(f_in, f_out)
                    print('Uncompressing finished.')
                else:
                    print('Skip download of ', ftp_file_name,'\n')
    
    #for gz_file in os.listdir(refseq_raw_data_folder):


def genbank_to_dict(db_dir, work_dir,re_load, id_list):
    # check for and use the newest RefSeq release
    release_no_list = []
    for release in os.listdir(db_dir):
        release_no_list.append(int(release.split('_')[-1]))
    newest_refseq_release = db_dir/str('Release_no_' + str(max(release_no_list))) / 'gunzipped'
    
    out_file=work_dir/'TEMP/python_dict_loaded_species.pkl'
    
    if out_file.parent.is_dir() == False:
        os.mkdir(out_file.parent)
    
    #write GenBank informations to dict and safe the dict as file
    if out_file.is_file() == False or re_load == True:
        print('Reading the RefSeq GenBank files.')
        species_dict = dict.fromkeys(id_list)
        for db_file in os.listdir(newest_refseq_release):
            if 'gbff' in db_file:
                with open(newest_refseq_release / db_file, 'r') as handle:
                    for record in Bio.GenBank.parse(handle):
                        for pid in id_list:
                            if pid in record.version:
                                species_dict[pid]=record
        print('Reading finished.','\n')
        with open(out_file, 'wb') as f:
            pickle.dump(species_dict, f)


def load_pep_from_dict(work_dir, crs_lists_lst, outgroup_sp_lst):
    path_to_dict = work_dir/'TEMP/python_dict_loaded_species.pkl'
    
    pep_files = work_dir/'01.pep_files'
    ref_file = work_dir/'07.References/References.txt'
    
    #if pep_files.is_dir() == False:
    #    os.mkdir(pep_files)
    if pep_files.is_dir() == True:
        shutil.rmtree(pep_files)
    os.mkdir(pep_files)
    
    if ref_file.parent.is_dir() == False:
        os.mkdir(ref_file.parent)
    
    #initialise citation file
    with open(ref_file, 'w') as out_ref:
        out_ref.write('##### The following references were used to build the tree.\n')
        out_ref.close()
    
    #load dict from file
    with open(path_to_dict, 'rb') as f:
        species_dict = pickle.load(f)
        f.close()

    for ncbi_id, record in species_dict.items():

        #harmonize crs_lists_lst (i.e. "translate" NCBI ids into species names)
        harmonized_crs_lists_lst = []
        for sublist in crs_lists_lst:
            for i,item in enumerate(sublist):
                if item == ncbi_id:
                    sublist[i] = record.organism.replace(' ','_')
            harmonized_crs_lists_lst.append(sublist)
        

        #harmonize outgroup_sp_lst (i.e. "translate" NCBI ids into species names)
        for i,item in enumerate(outgroup_sp_lst):
            if item == ncbi_id:
                outgroup_sp_lst[i] = record.organism.replace(' ','_')

        
        #append to citation file
        with open(ref_file, 'a') as out_ref:
            out_ref.write('\n\n# '+ncbi_id+'\t'+record.organism+'\n')
            for ref in record.references:
                out_ref.write(str(ref))
        
        #write pep fasta file
        with open(pep_files/str(record.organism.replace(' ','_')+'.pep.fasta'), 'w') as out:
            for f in record.features:
                if f.key == 'CDS':
                    for subf in f.qualifiers:
                        if subf.key == '/translation=':
                            out.write(str('>'+record.organism.replace(' ','_')+'\n'))
                            out.write(subf.value[1:-1]+'\n')
    return harmonized_crs_lists_lst, outgroup_sp_lst


def copy_to_pep_folder(work_dir,input_files_path):
    out_dir = work_dir/'01.pep_files'
    for file in os.listdir(input_files_path):
        #if file.endswith('.fasta'):
        #    shutil.copy(input_files_path/file,out_dir)
        shutil.copy(input_files_path/file,out_dir)
        print('File ',file, 'has successfully been accessed.')
    print()


def check_crs(work_dir, crs_lists_lst):
    in_dir = work_dir/'01.pep_files'
    all_sp_lst = []
    for file in os.listdir(in_dir):
        with open(in_dir/file, 'r') as f:
            line = f.readline()
            all_sp_lst.append(line.strip().split('>')[1])
        f.close()
    
    new_crs_lst = []
    for sublist in crs_lists_lst:
        new_sub_lst = []
        for element in sublist:
            if element in all_sp_lst:
                new_sub_lst.append(element)
            else:
                print('Alert! Check your configuration file. The species ',element,' or its corresponding ID is in the CRS section but not in the Define_species section or in the Input_file folder. For now it will not be taken into evaluation.','\n')
        if len(new_sub_lst) >= 2:
            new_crs_lst.append(new_sub_lst)
    return new_crs_lst


def numerate_ids_2(work_dir):
    #and delete EXACT paralogs and sequences with '*' and sequences shorter then 10 AAs
    in_dir = work_dir/'01.pep_files'
    out_dir = work_dir/'02.numerated_pep_files'
    #if out_dir.is_dir() == False:
    #    os.mkdir(out_dir)
    if out_dir.is_dir() == True:
        shutil.rmtree(out_dir)
    os.mkdir(out_dir)
    
    for file in os.listdir(in_dir):
        numerated_records_list = []
        seq_recordseq_list = []
        for i, seq_record in enumerate(Bio.SeqIO.parse(in_dir/file, "fasta")):
            if '*' not in seq_record.seq and len(seq_record.seq) > 9 and str(seq_record.seq) not in seq_recordseq_list:
                seq_recordseq_list.append(str(seq_record.seq))
                #print(seq_recordseq_list)
                numerated_records_list.append(Bio.SeqRecord.SeqRecord(seq_record.seq,id=seq_record.id+'_'+str(i),description=''))
        Bio.SeqIO.write(numerated_records_list, out_dir/file, "fasta-2line")


def call_orthofinder2(work_dir, of_exec_path):
    pep_files = work_dir/'02.numerated_pep_files'
    
    of_folder = work_dir/'03.Orthofinder'
    
    if of_folder.is_dir() == True:
        shutil.rmtree(of_folder) 
        
    p = subprocess.Popen( str(of_exec_path) + ' -f ' + str(pep_files) + ' -o ' + str(of_folder), shell=True)
    p.communicate()



# trimming of OF results


def trimm_of2_results( work_dir, abs_list, outgroupsp_list ):

    print('OrthoFinder2 finished. Now the results will be trimmed')

    #This is the main function, that is also calling the other functions
    #Its purpose is to iterate over each OG file, decides which to keep and which not. finally it will write the "ultimate" file (without paralogs, etc.)
    
    
    out_dir = work_dir/'04.trimmed_Orthogroups'    
    if out_dir.is_dir() == False:
        os.mkdir(out_dir)
    
    #search for newest Orhtofidner Results dir
    the_dir = work_dir/'03.Orthofinder'
    dirs = []
    for d in os.listdir(the_dir):
        if os.path.isdir(the_dir/d):
            dirs.append(the_dir/d)
    newest_of_folder = sorted(dirs, key=lambda x: os.path.getctime(x), reverse=True)[:1][0]

    
    of_results_dir = newest_of_folder/'Orthogroup_Sequences'
    of_tree_dir = of_results_dir.parent/'Gene_Trees'
    #print(type(of_tree_dir))
    
    file_list = os.listdir(of_results_dir)
    
    #iterate over every OG file
    for file in file_list:
        curr_file_tree_path = pathlib.Path(str(of_tree_dir)+'/'+(file.split('.')[0]+'_tree.txt'))
        
        og_file = SeqIO.parse(of_results_dir/file, "fasta")
        
        #could be nicer
        species_list=[]
        for seq_entry in og_file:
            if seq_entry.id not in species_list:
                species_list.append(seq_entry.id)
                #print(type(species_list[0]))
            
        
        #really necessary?
        #read the fasta file into an pandas dataframe
        with open(of_results_dir/file) as fp:
            records = [{'species': str(record.description), 
                        'sequence': str(record.seq)} for record in SeqIO.parse(fp,"fasta")]
            df = pd.DataFrame.from_records(records)
            
        
        # removal of paralogs with the exact same sequence
        #df = df.drop_duplicates()
        #species_list = df['species'].drop_duplicates().to_list()
        
        # resetting the boolean, which is stating, if the current OG file will be "discarded" or keept
        drop_OG = False
        
        print('\nCurrently',file,' is being trimmed.')

        # 1. main step: removal of outlier sequences within an orthogroup, identified via phylogenetic relations
        remove_list = []
        if curr_file_tree_path.is_file():
            #drop_OG, outlier_list = find_outliers_in_tree( curr_file_tree_path, outgroupsp_list, abs_list )
            outlier_list = find_outliers_in_tree( curr_file_tree_path, outgroupsp_list )
            for element in species_list:
                for outlier in outlier_list:
                    #print(element)
                    #print(outlier_list)
                    if element in outlier:
                        remove_list.append(element)
            species_list_2 = species_list
            species_list = [i for i in species_list_2 if i not in remove_list]
        
        if len(remove_list) > 0:
            for element in remove_list:
                print(element,' is an outleir and has been removed.')
        
        # 2. main step: checking if the closely related species of interest are both represented in the current OG, if not, the whole OG is dropped
        for sublist in abs_list:
            if related_species_present_in_OG_checker(species_list, sublist ) == False:
                print('The whole Orthogroup is beeing discarded, after not every of the defined closely related species are present/absent.')
                drop_OG = True
        
        # 3. main step: The following lines are dropping small files with just a few or just one sequence, that are not from one of the "outgroup" species
        if len(species_list) <= len(outgroupsp_list):
            for species in species_list:
                #print('_'.join(species.split('_')[0:-1]))
                if '_'.join(species.split('_')[0:-1]) not in outgroupsp_list and drop_OG == False:
                    #print(outgroupsp_list)
                    print('The whole Orthogroup is beeing discarded, because it is too small and does not contain only outgroup species sequences.')
                #if species not in outgroupsp_list:
                    drop_OG = True
    
        
        # if the og file will be kept the following lines are checking the sequences themself and then write the new files
        if drop_OG == False:
            with open(out_dir/file, 'w') as out:
                
                #iteration over each species in the file (it will be only iterated ones, even if presented mutliple times in the file)
                for species in species_list:
                    out.write('>'+'_'.join(species.split('_')[0:-1])+'\n')
                    
                    # if the species is only present onces
                    if len(df.loc[df['species'] == species].index) == 1:
                        sequence = df.loc[df['species'] == species]['sequence'].to_list()
                        out.write(sequence[0]+'\n')
                        
                    #if the species is represented at least twice in this OG
                    if len(df.loc[df['species'] == species].index) >= 2: # checks, if another paralog occurs and does something
                        diff_para_list = df.loc[df['species'] == species]['sequence'].to_list()
                        out.write(max(diff_para_list)+'\n') # max() is choosing the longest sequence


def find_outliers_in_tree( treefile, outgroup_sp ):
    '''
    This function opens a given tree file (from Orthofinder results) and finds
    statistical outliers by looking at taxon distance (outliers are only
    identified within the non-outgroup classified species, therefore the
    outgroup-species list is also inculded)
    '''
    
    # Newick tree format has just one line in a text file, so we can read this line into a string
    with open(treefile, 'r') as f:
        str_tree = f.readline().replace(' ','_')
        f.close()
    
    #now we can open the tree with dendropy
    tree = dendropy.Tree.get_from_string(str_tree, 'newick')
    
    #species name and its distance into a dictionary
    df = {}
    for nd in tree.postorder_edge_iter():
        if nd.length is None:
            continue
        taxn = nd.head_node.taxon
        if taxn:
            df[taxn.label] = nd.length

    #dictionary to pandas dataframe
    pd_df = pd.DataFrame(df.items(), columns=['species', 'distance'])

    # removal of outgroup species (only for the calculations for the outlier statistics)
    outlier_rm_ls = list()
    for i in range(len(pd_df['species'])):
        for element in outgroup_sp:
            if element in str(pd_df['species'][i]).replace(' ','_'):
                outlier_rm_ls.append(i)
    pd_df_cut = pd_df.drop(labels=outlier_rm_ls)

    outputlist=[]
    if len(pd_df_cut) > 0:

        #outlier calculations with numpy percentile (standard method is linear)
        Q1 = np.percentile(pd_df_cut['distance'].values, 25)
        Q3 = np.percentile(pd_df_cut['distance'].values, 75)
        IQR = Q3 - Q1
        upper=Q3+1.5*IQR
    
        #the species that should be removed from an output (because they are outliers) will be appended to the output list
        for element in pd_df_cut.loc[pd_df_cut['distance'] > upper]['species']:
            outputlist.append(element.replace(' ','_'))
            #outputlist.append(element)

    return outputlist
    

def related_species_present_in_OG_checker(species_list, sp_list ):
    count=0
    for crs in sp_list:
        for element in species_list:
            if crs == '_'.join(element.split('_')[0:-1]):
                count = count + 1
    #print(count)
    #print(len(sp_list))
    if count >= len(sp_list) or count == 0:
        return True
    else:
        return False



# aliugnment and tree calculation


def mafft_loop(work_dir):
    in_dir = work_dir/'04.trimmed_Orthogroups'
    out_dir = work_dir/'05.MAFFT'
    file_list = os.listdir(in_dir)
    if out_dir.is_dir() == False:
        os.mkdir(out_dir)
    for file in file_list:
        p = subprocess.Popen( 'mafft --maxiterate 1000 --localpair ' + str(in_dir/file) + '>' + str(out_dir/file) + '.ali.fasta', shell=True)
        p.communicate()
    #p = subprocess.Popen( 'python3 /vol/data/samuel/plastome/scripts/umra_v2.py ' + out_dir, shell=True)
    #p.communicate()


def concatenation( work_dir, iqtree_exec_path, iqtree_params, no_tree ):
    in_dir = work_dir/'05.MAFFT'
    out_dir = work_dir/'06.Tree'
    if out_dir.is_dir() == False:
        os.mkdir(out_dir)
    aln_collection = {}
    species_list = []
    aln_len_per_gene = {}	#length of clean alignment per gene
    file_list = os.listdir(in_dir)
    for file in file_list:
        alignment = special_alignment_load( in_dir/file )
        aln_collection.update( { str(file).strip().split('/')[-1]:  alignment } )
        species_list += alignment.keys()
        aln_len_per_gene.update( { str(file).strip().split('/')[-1]: len( list( alignment.values() )[0] ) } )
    #for file in file_list:
        
        
	# --- merge all clean alignments and fill species gaps --- #
    species_list = list( sorted( list( set( species_list ) ) ) )	#sorted list of all species of interest
    genes = list( sorted( aln_collection.keys() ) )		#sorted list of all genes for tree construction
    data_per_species = {}
    for spec in species_list:
        data_per_species.update( { spec: [] } )
	        
        
    partition_lengths = []
    #for gene in genes[:min( [ num_of_alignments, len( genes )-1 ] )]:	#only use given number of alignments for tree construction
    for gene in genes:	#only use given number of alignments for tree construction
        for k, spec in enumerate( species_list ):
            if k == 0:
                partition_lengths.append( aln_len_per_gene[ gene ] )
            try:
                seq = aln_collection[ gene ][ spec ].upper()
            except KeyError:
                seq = "-" * aln_len_per_gene[ gene ]
            data_per_species[ spec ].append( seq.strip() )
   	
   	# --- construct tree --- #
    final_alignment_output_file = out_dir / "final_aln.fasta"
    with open( final_alignment_output_file, "w" ) as out:
        for spec in species_list:
            out.write( '>' + spec.replace( "_cluster", "" ).replace( "_trinity", "" ).replace( "_unigene", "" ) + "\n" + "".join( data_per_species[ spec ] ) + "\n" )
    out.close()
    
    if no_tree:
        p = subprocess.Popen( str(iqtree_exec_path) + ' -s ' + str(final_alignment_output_file) + ' ' + iqtree_params, shell=True)
        p.communicate()


def special_alignment_load( fasta_file ):
    # from boas script "tree_consctructor ..."
	"""! @brief load candidate gene IDs from file """
	
	sequences = {}
	with open( fasta_file ) as f:
		header = f.readline()[1:].strip()
		if "@" in header:
			header = header.split('@')[0]
		seq = []
		line = f.readline()
		while line:
			if line[0] == '>':
				seq = "".join( seq ).replace("\x00", "-")
				if not "\x00" in seq:
					sequences.update( { header: seq } )
				else:
					print ( header)
					print ( [ seq ])
				header = line.strip()[1:]
				if "@" in header:
					header = header.split('@')[0]
				seq = []
			else:
				seq.append( line.strip() )
			line = f.readline()
		seq = "".join( seq ).replace("\x00", "-")
		if not "\x00" in seq:
			sequences.update( { header: seq } )	
		else:
			print ( header)
			print ( [ seq ] )
	return sequences




argument_using_and_function_caller( args )
print('done')
