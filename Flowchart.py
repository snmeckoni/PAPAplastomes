#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Flowchart for PAPAplastomes v.01

import graphviz


def generate_graph():
    dot = graphviz.Digraph()
    dot.graph_attr["style"]="rounded"
    
    dot.node('NCBI', 'NCBI-RefSeq data', shape='box', style='filled,rounded', fillcolor='palegreen2')
    dot.node('gpep', 'extracted peptide sequences', shape='box', style='filled,rounded', fillcolor='palegreen2')
    dot.node('opep', 'peptide sequences from own assemblies', shape='box', style='filled,rounded', fillcolor='palegreen2')
    dot.node('preot', 'pre-OrthoFinder trimming', shape='box', style='filled,rounded', fillcolor='lightpink')
    dot.node('postot', 'post-OrthoFinder trimming', shape='box', style='filled,rounded', fillcolor='lightpink') 
    dot.node('ot', 'OrthoFinder', shape='box', style='filled,rounded', fillcolor='khaki')
    dot.node('mafft', 'MAFFT', shape='box', style='filled,rounded', fillcolor='khaki')
    dot.node('iq', 'IQ-TREE', shape='box', style='filled,rounded', fillcolor='khaki')
    #dot.node('ex', 'external Tool', shape='box', style='filled,rounded', fillcolor='khaki')
    #,style='invis'

    dot.edge('opep','preot')
    dot.edge('NCBI','gpep')
    dot.edge('gpep','preot')
    dot.edge('preot','ot')
    dot.edge('ot','postot')
    dot.edge('postot','mafft')
    dot.edge('mafft','iq')

    dot.render('flowchart.gv', format='png', view=True)  # doctest: +SKIP
    #dot.render('dot', 'png', 'flowchart.gv').

generate_graph()
