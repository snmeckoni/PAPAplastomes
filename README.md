# PAPAplastome - Pipeline for the Automatic Phylogenetic Analysis of plastomes

This program incorporates your newly sequenced and annotated plastome data into a phylogenetic tree with reference data from the NCBI RefSeq database which is automatically downloaded.

```
usage: PAPAplastome.py [-h] [-n] [-d] [-l] INPUT CONFIG

	Description:
	PAPAplastome v.01 - This program incorporates your newly sequenced and annotated plastome data into a phylogenetic tree with reference data from the NCBI RefSeq database which is automatically downloaded.

positional arguments:
  INPUT              Path to working directory folder
  CONFIG             Path to config file

options:
  -h, --help         show this help message and exit
  -n, --no_tree      Do not calculate a phylogenetic tree.
  -d, --re_download  Downloads the RefSeq files even if they are present. This can be helpful if the integrity of the RefSeq files might be corrupted.
  -l, --re_load      Re-Loads the data from the RefSeq files. This should be conducted if the config file was modified. This will be automatically enabled if -d is used.
```
### example config file:
```
# This is a config file for PAPAplastome
# commentlines begin with a hashtag

[Paths]
#specify the OrthoFinder path:
OrthoFinder = /path/to/OrthoFinder/orthofinder

#specify the IQ-Tree path, the given example is fulfilling the requirements if installed via apt on an ubuntu machine
IQ-Tree = iqtree2
#IQ-Tree = /example/path/to/iqtree-1.6.12-Linux/bin/iqtree

#path to species_of_interest folder containing
Input_files = /path/to/folder/containing/.fasta/files/

#With the Alternative_NCBI_RefSeq_link you can apply this programm to other kind of sequences and species, standard value is the ones for plastomes, others were not tested
NCBI_RefSeq_link = https://ftp.ncbi.nlm.nih.gov/refseq/release/plastid/
#NCBI_RefSeq_link = ''


[Define_species]
#Make sure you always give the full NCBI-ID (with the dot and following number at the end). Only use NCBI IDs
#You have to seperate the IDs by a comma, but spaces are optional

#The Reference_species are later placed inside the tree with the species of interest.
Reference_species = NC_039434.1,NC_034688.1,NC_039707.1,NC_029770.1,NC_007977.1,NC_034689.1,NC_041435.1,NC_039621.1,NC_053698.1,NC_032057.1,NC_035632.2,NC_005086.1,NC_009962.1,NC_005353.1,NC_018117.1,NC_016986.1,NC_010109.1,NC_061549.1,NC_065468.1,NC_058936.1,NC_001879.2,NC_007898.3,NC_008096.2,NC_021449.1,NC_001666.2,NC_034950.1,NC_021456.1,NC_032367.1,NC_068046.1
#pro tip: add a commentline to note the NCBI IDs to the species names

#The "Outgroup_species" do not have to be mentioned again at the Reference_species, but it is recommended because you can easily change the outgroup species without taken a species not into account if you forget to also add it to the Reference_species 
Outgroup_species = NC_005353.1,NC_016986.1,NC_005086.1,NC_021456.1,NC_032367.1


[CRS]
#close related species, use NCBI IDs for reference species. For species of interest use the name that is present in the header of their sequences. Make sure to use an underscore instead of a space. To define CRSs is important for trimming step 2 and mandatory for incorporation of own data.
this_name_as_you_want_doesnt_mater_anyways = NC_068046.1,NC_034688.1,Digitalis_purpurea2
ceratophylaceae = NC_009962.1,Ceratophyllum_submersum
caryos = Beta_nana,Beta_vulgaris
Picea = NC_021456.1,NC_032367.1
example = Examplus_specius, another_example
```
### Tips
If you want to incorporate species, that are not in the NCBI RefSeq database or if you want to use your own data, make sure to use the following structure. This example is given for the species Examplus specius:
1. Prepare the peptide fasta files and name all the headers like the file was named (and without spaces):

    Examplus_specius.fasta
    ```
    >Examplus_specius
    GAVAKSAQ
    >Examplus_specius
    AFLSVT...
    >Examplus_specius
    ...
    ```
2. Give the species name in the config file as the header/fasta-file was named. You have to put it under the CRS section with an appropriate close related reference. In this example:
    ```
    Examplus_specius
    ```
3. Place all the peptide files of your species of interest into one folder give the path of this folder in the config file

## PAPAplastomes v.01 - Flowchart
![A flowchart of the program PAPAplastomes v.01 representing the basic workflow of the program.](flowchart.gv.png)
